package com.kyanogen.signatureview.interfaces

interface ISignatureStart {
    fun onSignatureStarted()
}