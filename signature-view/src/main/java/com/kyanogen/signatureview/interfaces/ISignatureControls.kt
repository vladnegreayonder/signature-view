package com.kyanogen.signatureview.interfaces

interface ISignatureControls {
    /**
     * Get color for clear button
     *
     * @return int
     */
    fun getClearButtonColor(): Int

    /**
     * Set color for clear button
     *
     * @param clearButtonColor int
     */
    fun setClearButtonColorColor(penColor: Int)
}