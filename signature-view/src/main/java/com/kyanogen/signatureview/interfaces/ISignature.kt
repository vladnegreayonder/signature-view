package com.kyanogen.signatureview.interfaces

import android.graphics.Bitmap

interface ISignature {
    fun getSignatureBitmap(): Bitmap
    fun setBitmap(bitmap: Bitmap)
    fun isBitmapEmpty(): Boolean
    fun getVersionName(): String

    /**
     * Get stoke size for signature creation
     *
     * @return float
     */
    fun getPenSize(): Float

    /**
     * Set stoke size for signature creation
     *
     * @param penSize float
     */
    fun setPenSize(penSize: Float)

    /**
     * Check if drawing on canvas is enabled or disabled
     *
     * @return boolean
     */
    fun isEnableSignature(): Boolean

    /**
     * Enable or disable drawing on canvas
     *
     * @param enableSignature boolean
     */
    fun setEnableSignature(enableSignature: Boolean)

    /**
     * Get stoke color for signature creation
     *
     * @return int
     */
    fun getPenColor(): Int

    /**
     * Set stoke color for signature creation
     *
     * @param penColor int
     */
    fun setPenColor(penColor: Int)

    /**
     * Get background color
     *
     * @return int
     */
    fun getBackgroundColor(): Int

    /**
     * Set background color
     *
     * @param backgroundColor int
     */
    fun setBackgroundColor(backgroundColor: Int)

    /**
     * Clear signature from canvas
     */
    fun clearCanvas()
}