package com.kyanogen.signatureview.ui

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.kyanogen.signatureview.interfaces.ISignature
import com.kyanogen.signatureview.R
import com.kyanogen.signatureview.interfaces.ISignatureControls
import com.kyanogen.signatureview.interfaces.ISignatureStart
import kotlinx.android.synthetic.main.signature_parent_view.view.*


class SignatureViewWithControls : FrameLayout, ISignature, ISignatureStart, ISignatureControls {
    private var _clearButtonColor: Int = context.resources.getColor(R.color.penRoyalBlue)

    override fun getClearButtonColor(): Int = _clearButtonColor

    override fun setClearButtonColorColor(penColor: Int) {
        _clearButtonColor = context.resources.getColor(penColor)
        clear_button.setTextColor(_clearButtonColor)
    }

    override fun onSignatureStarted() {
        textView.visibility = View.GONE
    }

    override fun getSignatureBitmap(): Bitmap {
        return signatureView.getSignatureBitmap()
    }

    override fun setBitmap(bitmap: Bitmap) {
        signatureView.setBitmap(bitmap)
    }

    override fun isBitmapEmpty(): Boolean {
        return signatureView.isBitmapEmpty()
    }

    override fun getVersionName(): String {
        return signatureView.getVersionName()
    }

    override fun getPenSize(): Float {
        return signatureView.getPenSize()
    }

    override fun setPenSize(penSize: Float) {
        signatureView.setPenSize(penSize)
    }

    override fun isEnableSignature(): Boolean {
        return signatureView.isEnableSignature()
    }

    override fun setEnableSignature(enableSignature: Boolean) {
        signatureView.setEnableSignature(enableSignature)
    }

    override fun getPenColor(): Int {
        return signatureView.getPenColor()
    }

    override fun setPenColor(penColor: Int) {
        signatureView.setPenColor(penColor)
    }

    override fun getBackgroundColor(): Int {
        return getBackgroundColor()
    }

    override fun clearCanvas() {
        signatureView.clearCanvas()
        textView.visibility = View.VISIBLE
    }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        View.inflate(context, R.layout.signature_parent_view, this)

        signatureView.setBackgroundColor(Color.TRANSPARENT)
        signatureView.signatureStartListener = this

        clear_button.setOnClickListener { clearCanvas() }

        val typedArray = context.theme.obtainStyledAttributes(
                attrs, R.styleable.signature, 0, 0)

        try {
            val penColor = typedArray.getColor(R.styleable.signature_penColor,
                    context.resources.getColor(R.color.penRoyalBlue))
            val penSize = typedArray.getDimension(R.styleable.signature_penSize,
                    context.resources.getDimension(R.dimen.pen_size))
            val enableSignature = typedArray.getBoolean(R.styleable.signature_enableSignature, true)

            _clearButtonColor = typedArray.getColor(R.styleable.signature_clearButtonColor,
                    context.resources.getColor(R.color.penRoyalBlue))

            clear_button.setTextColor(_clearButtonColor)

            signatureView.setPenColor(penColor)
            signatureView.setPenSize(penSize)
            signatureView.setEnableSignature(enableSignature)
        } finally {
            typedArray.recycle()
        }
    }
}
